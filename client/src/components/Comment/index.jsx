import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import Like from '../Like';
import Dislike from '../Dislike';
import styles from './styles.module.scss';

const Comment = ({
  likeComment,
  dislikeComment,
  comment: {
    body,
    createdAt,
    user,
    id,
    isLike,
    likeCount,
    isDislike,
    dislikeCount
  }
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
      <CommentUI.Metadata>{moment(createdAt).fromNow()}</CommentUI.Metadata>
      <CommentUI.Text>{body}</CommentUI.Text>
      <CommentUI.Action>
        <div className={styles.commentReaction}>
          <Like
            id={id}
            handleLike={likeComment}
            likeCount={likeCount}
            isLike={isLike}
          />
          <Dislike
            id={id}
            handleDislike={dislikeComment}
            dislikeCount={dislikeCount}
            isDislike={isDislike}
          />
        </div>
      </CommentUI.Action>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

export default Comment;
