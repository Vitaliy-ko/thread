import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Dislike = ({ id, handleDislike, dislikeCount, isDislike }) => (
  <Label
    color={isDislike ? 'blue' : 'black'}
    basic
    size="small"
    as="a"
    className={styles.dislikeLabel}
    onClick={() => handleDislike(id)}
  >
    <Icon name="thumbs down" />
    {dislikeCount}
  </Label>
);

Dislike.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  handleDislike: PropTypes.func.isRequired,
  dislikeCount: PropTypes.number.isRequired,
  isDislike: PropTypes.bool
};

Dislike.defaultProps = {
  isDislike: false
};

export default Dislike;
