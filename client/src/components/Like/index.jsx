import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Like = ({ id, handleLike, likeCount, isLike }) => (
  <Label
    color={isLike ? 'blue' : 'black'}
    basic
    size="small"
    as="a"
    className={styles.likeLabel}
    onClick={() => handleLike(id)}
  >
    <Icon name="thumbs up" />
    {likeCount}
  </Label>
);

Like.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  handleLike: PropTypes.func.isRequired,
  likeCount: PropTypes.number.isRequired,
  isLike: PropTypes.bool
};

Like.defaultProps = {
  isLike: false
};

export default Like;
