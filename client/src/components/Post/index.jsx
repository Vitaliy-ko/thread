import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, TextArea, Button } from 'semantic-ui-react';
import moment from 'moment';

import Like from '../Like';
import Dislike from '../Dislike';
import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  deletePost,
  updatePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    isLike,
    isDislike,
    isOwner
  } = post;
  const date = moment(createdAt).fromNow();

  const [isEdit, setIsEdit] = useState(false);
  const [postBody, setPostBody] = useState(body);

  const handleApplyPostBody = async () => {
    setIsEdit(false);
    await updatePost({ id, body: postBody });
  };

  const handleDeletePost = async postId => {
    toggleExpandedPost();
    await deletePost(postId);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <div className={styles.contentContainer}>
          <Card.Meta>
            <span className="date">
              posted by&nbsp;
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          {isOwner && (
            <div>
              <Label
                basic
                size="small"
                as="a"
                className={[styles.toolbarBtn, styles.editLabel].join(' ')}
                onClick={() => handleDeletePost(id)}
              >
                <Icon
                  className={styles.editIcon}
                  tag="a"
                  corner="top right"
                  name="trash"
                />
              </Label>
              <Label
                basic
                size="small"
                as="a"
                className={[styles.toolbarBtn, styles.editLabel].join(' ')}
                onClick={() => setIsEdit(true)}
              >
                <Icon
                  className={styles.editIcon}
                  tag="a"
                  corner="top right"
                  name="pencil alternate"
                />
              </Label>
            </div>
          )}
        </div>
        {isEdit ? (
          <TextArea
            value={postBody}
            onChange={e => setPostBody(e.target.value)}
            className={styles.editBody}
          />
        ) : (
          <Card.Description>{postBody}</Card.Description>
        )}
      </Card.Content>
      <Card.Content extra>
        <div className={styles.contentContainer}>
          <div>
            <Like
              id={id}
              handleLike={likePost}
              likeCount={Number(likeCount)}
              isLike={isLike}
            />
            <Dislike
              id={id}
              handleDislike={dislikePost}
              dislikeCount={Number(dislikeCount)}
              isDislike={isDislike}
            />
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => toggleExpandedPost(id)}
            >
              <Icon name="comment" />
              {commentCount}
            </Label>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => sharePost(id)}
            >
              <Icon name="share alternate" />
            </Label>
          </div>
          {isEdit && (
            <div>
              <Button color="blue" size="tiny" onClick={handleApplyPostBody}>
                Apply
              </Button>
              <Button
                size="tiny"
                onClick={() => {
                  setIsEdit(false);
                  setPostBody(body);
                }}
              >
                Cancel
              </Button>
            </div>
          )}
        </div>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  isLike: PropTypes.bool,
  isDislike: PropTypes.bool
};

Post.defaultProps = {
  isLike: false,
  isDislike: false
};
export default Post;
