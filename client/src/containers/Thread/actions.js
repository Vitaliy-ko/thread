import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_UPDATED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const updatePostAction = posts => ({
  type: SET_UPDATED_POST,
  posts
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const updatePost = ({ id, body }) => async (dispatch, getRootState) => {
  const { id: postId } = await postService.updatePost({ id, body });
  const updatedPost = await postService.getPost(postId);

  const { posts: { posts } } = getRootState();
  const updatedPosts = posts.map(post => {
    if (post.id !== id) {
      return post;
    }
    return updatedPost;
  });

  dispatch(updatePostAction(updatedPosts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  newPost.isOwner = true;
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { deletedCount } = await postService.deletePost({ id: postId });
  if (deletedCount !== 1) {
    return;
  }

  const { posts: { posts } } = getRootState();
  const remainingPosts = posts.filter(post => post.id !== postId);
  dispatch(setPostsAction(remainingPosts));
};

const mapLikes = post => {
  let result;
  if (post.isLike) {
    result = {
      ...post,
      likeCount: Number(post.likeCount) - 1,
      isLike: false,
      isDislike: false
    };
  } else if (post.isDislike) {
    result = {
      ...post,
      likeCount: Number(post.likeCount) + 1,
      dislikeCount: Number(post.dislikeCount) - 1,
      isLike: true,
      isDislike: false
    };
  } else {
    result = {
      ...post,
      likeCount: Number(post.likeCount) + 1,
      isLike: true,
      isDislike: false
    };
  }
  return result;
};

const mapDislikes = item => {
  let result;

  if (item.isDislike) {
    result = {
      dislikeCount: Number(item.dislikeCount) - 1,
      isDislike: false
    };
  } else if (item.isLike) {
    result = {
      likeCount: Number(item.likeCount) - 1,
      dislikeCount: Number(item.dislikeCount) + 1,
      isLike: false,
      isDislike: true
    };
  } else {
    result = {
      dislikeCount: Number(item.dislikeCount) + 1,
      isDislike: true
    };
  }
  return result;
};

const reactPost = async (mapPosts, postId, dispatch, getRootState) => {
  let changedPost;

  const setChangedPost = post => {
    const reaction = mapPosts(post);
    changedPost = { ...post, ...reaction };
    return changedPost;
  };

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : setChangedPost(post)));
  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }

  await postService.reactPost(changedPost);
};

export const likePost = postId => (dispatch, getRootState) => reactPost(mapLikes, postId, dispatch, getRootState);

export const dislikePost = postId => (dispatch, getRootState) => reactPost(mapDislikes, postId, dispatch, getRootState);

const reactComment = async (mapComments, commentedPost, commentId) => {
  const { comments } = commentedPost;
  console.log('comments', comments);
  const reactedComment = comments.find(comment => comment.id === commentId);
  const reaction = mapComments(reactedComment);

  await commentService.reactComment({ ...reactedComment, ...reaction });
};

export const likeComment = commentedPost => commentId => dispatch => {
  console.log('commentedPost', commentedPost);
  reactComment(mapLikes, commentedPost, commentId, dispatch);
};

export const dislikeComment = commentedPost => commentId => dispatch => (
  reactComment(mapDislikes, commentedPost, commentId, dispatch)
);

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
