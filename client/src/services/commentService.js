import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const reactComment = async ({ id, isLike, isDislike }) => {
  const response = await callWebApi({
    endpoint: '/api/comments/react',
    type: 'PUT',
    request: {
      commentId: id,
      isLike,
      isDislike
    }
  });
  return response.json();
};

export const updateComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'PUT',
    request
  });

  return response.json();
};

export const deletePost = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'DELETE',
    request
  });

  return response.json();
};
