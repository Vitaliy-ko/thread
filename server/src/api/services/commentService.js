import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const setReaction = async (userId, { commentId, isLike, isDislike }) => {
  const updateOrDelete = react => ((isLike === isDislike)
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike, isDislike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike, isDislike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : commentReactionRepository.getPostReaction(userId, commentId);
};
