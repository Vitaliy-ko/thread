import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = (filter, currentUserId) => postRepository.getPosts(filter, currentUserId);

export const getPostById = (id, currentUserId) => postRepository.getPostById(id, currentUserId);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const updatePost = async ({ id, body }) => postRepository.updateById(id, { body });

export const setReaction = async (userId, { postId, isLike, isDislike }) => {
  const updateOrDelete = react => ((isLike === isDislike)
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike, isDislike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike, isDislike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const deletePostById = id => postRepository.deleteById(id);
