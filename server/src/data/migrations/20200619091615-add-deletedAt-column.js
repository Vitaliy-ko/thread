module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize.transaction(t => Promise.all([
    queryInterface.addColumn('posts', 'deletedAt', {
      type: Sequelize.DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    }, { transaction: t })
  ])),
  down: queryInterface => queryInterface.sequelize.transaction(t => Promise.all([
    queryInterface.removeColumn('posts', 'deletedAt', { transaction: t })
  ]))
};
