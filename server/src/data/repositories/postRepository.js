import sequelize from '../db/connection';
import { PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const postLikeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

// const commentLikeCase = bool => `
//  CASE WHEN "commentReactions"."isLike" = ${bool}
//  THEN 1 ELSE 0 END
//  `;

class PostRepository extends BaseRepository {
  async getPosts(filter, currentUserId) {
    const {
      from: offset,
      count: limit,
      userId
    } = filter;

    const where = {};
    if (userId) {
      Object.assign(where, { userId });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`CASE WHEN "post"."userId" = '${currentUserId}' THEN true ELSE false END`), 'isOwner'],
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.literal(`
                        (SELECT "isLike"
                        FROM "postReactions"
                        WHERE "postReactions"."userId" = '${currentUserId}' AND
                        "post"."id" = "postReactions"."postId")`), 'isLike'],
          [sequelize.literal(`
                        (SELECT "isDislike"
                        FROM "postReactions"
                        WHERE "postReactions"."userId" = '${currentUserId}' AND
                        "post"."id" = "postReactions"."postId")`), 'isDislike'],
          [sequelize.fn('SUM', sequelize.literal(postLikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(postLikeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id, currentUserId) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'comments->commentReactions.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`CASE WHEN "post"."userId" = '${currentUserId}' THEN true ELSE false END`), 'isOwner'],
          [sequelize.literal(`
                            (SELECT "isLike"
                            FROM "postReactions"
                            WHERE "postReactions"."userId" = '${currentUserId}' AND
                            "post"."id" = "postReactions"."postId")`), 'isLike'],
          [sequelize.literal(`
                            (SELECT "isDislike"
                            FROM "postReactions"
                            WHERE "postReactions"."userId" = '${currentUserId}' AND
                            "post"."id" = "postReactions"."postId")`), 'isDislike'],
          [sequelize.literal(`
                            (SELECT COUNT(*)
                            FROM "comments" as "comment"
                            WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(postLikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(postLikeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: [
          'id',
          'body',
          'createdAt',
          'updatedAt',
          [sequelize.literal(`CASE WHEN "comments"."userId" = '${currentUserId}' THEN true ELSE false END`),
            'isOwner'],
          [sequelize.literal(`
                        (SELECT "isLike"
                        FROM "commentReactions"
                        WHERE "commentReactions"."userId" = '${currentUserId}' AND
                        "comments"."id" = "commentReactions"."commentId")`), 'isLike'],
          [sequelize.literal(`
                        (SELECT "isDislike"
                        FROM "commentReactions"
                        WHERE "commentReactions"."userId" = '${currentUserId}' AND
                        "comments"."id" = "commentReactions"."commentId")`), 'isDislike']
        ],
        include: [{
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: CommentReactionModel,
          attributes: [
            'id',
            'isLike'
            // [sequelize.fn('SUM', sequelize.literal(commentLikeCase(true))), 'likeCount']
          ]
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
